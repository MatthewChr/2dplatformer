﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// Author: Matthew Christiansen
/// email: 17490@my4county.net
/// <summary>
/// Selects the level to load to
/// </summary>
public class LevelSelector : MonoBehaviour
{
    [SerializeField] float distance;
    [SerializeField] bool isOpen;
    [SerializeField] GameObject levelSelectMenu;
    [SerializeField] bool level2Unlocked;
    [SerializeField] GameObject xSprite;
    [SerializeField] Button button;
    Transform player;
    /// <summary>
    /// runs on start
    /// </summary>
    private void Start()
    {
        levelSelectMenu.SetActive(false);
        level2Unlocked = SaveSystem.GetSaveData().level2;

        player = FindObjectOfType<PlayerMovement>().transform;

        xSprite.SetActive(!level2Unlocked);
        button.enabled = level2Unlocked;
    }
    /// <summary>
    /// runs every frame
    /// </summary>
    void Update()
    {
        if (!isOpen)
        {
            if (Input.GetKeyDown(KeyCode.E)) 
            {
                if (Vector3.Distance(transform.position, player.position) <= distance)
                {
                    levelSelectMenu.SetActive(true);
                    isOpen = true;
                }
            }
        }
    }
    /// <summary>
    /// closes menu
    /// </summary>
    public void CloseMenu()
    {
        isOpen = false;
        levelSelectMenu.SetActive(false);
    }
    /// <summary>
    /// loads level at index "<paramref name="index"/>"
    /// </summary>
    /// <param name="index"></param>
    public void LoadLevel(int index)
    {
        SceneManager.LoadScene(index);
    }
    /// <summary>
    /// debug tool
    /// </summary>
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, distance);
    }
}
