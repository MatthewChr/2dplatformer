﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

/// Author: Matthew Christiansen
/// email: 17490@my4county.net
/// <summary>
/// Handles the trading and dialouge of the merchant
/// </summary>
public class Merchant : MonoBehaviour
{
    [TextArea(1, 4)]
    [SerializeField] string[] dialouge;
    [SerializeField] TextMeshProUGUI dialougeTxt;
    [SerializeField] float distance;
    [SerializeField] bool isOpen;
    [SerializeField] GameObject menu;
    [SerializeField] Color activeColor, inactiveColor;
    [SerializeField] Button btnSteam;
    [SerializeField] Image btnBG;
    [SerializeField] TextMeshProUGUI salvageCountTxt;
    PlayerAbilities playerData;
    Transform player;
    /// <summary>
    /// runs on start
    /// </summary>
    private void Start()
    {
        menu.SetActive(false);
        playerData = FindObjectOfType<PlayerAbilities>();

        player = FindObjectOfType<PlayerMovement>().transform;
    }
    /// <summary>
    /// runs every frame
    /// </summary>
    private void Update()
    {
        if (!isOpen)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (Vector3.Distance(transform.position, player.position) <= distance)
                {
                    menu.SetActive(true);
                    isOpen = true;

                    //checks if player has enough salvage to buy an upgrade and then enables/disables button
                    UpdateButton();
                    ShowRandomDialouge();
                }
            }
        }
        else
        {
            GameSaveData data = playerData.GetData();
            salvageCountTxt.text = "Current Salvage: " + data.salvageCount.ToString();
        }
    }
    /// <summary>
    /// Shows a random dialouge to the text component
    /// </summary>
    public void ShowRandomDialouge()
    {
        dialougeTxt.text = dialouge[Random.Range(0, dialouge.Length)];
    }
    /// <summary>
    /// closes menu
    /// </summary>
    public void CloseMenu()
    {
        isOpen = false;
        menu.SetActive(false);
    }
    /// <summary>
    /// activates and deactivates the button to buy upgrades if the player doesnt have enought salvage
    /// </summary>
    private void UpdateButton()
    {
        GameSaveData data = playerData.GetData();
        btnSteam.enabled = data.salvageCount >= 10;
        btnBG.color = data.salvageCount > 10 ? activeColor : inactiveColor;
    }
    /// <summary>
    /// debuging tool
    /// </summary>
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, distance);
    }
    /// <summary>
    /// exchanges salvage for steam fuel capacity
    /// </summary>
    public void BuySteamUpgrade()
    {
        GameSaveData data = playerData.GetData();
        data.salvageCount -= 10;
        data.jetpackFuel += 10;
        playerData.SetData(data);
        playerData.SaveCurrentData();
        UpdateButton();
    }
}
