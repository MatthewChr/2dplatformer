﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// Author: Matthew Christiansen
/// email: 17490@my4county.net
/// <summary>
/// Allows the use of animation events to change values on PlayerAbilities
/// </summary>
public class PlayerAnimEvents : MonoBehaviour
{
    [SerializeField] PlayerAbilities abilities;
    /// <summary>
    /// sets the hitbox enabled state to <paramref name="value"/>
    /// </summary>
    /// <param name="value"></param>
    public void MeleeHitboxEnabled(int value)
    {
        abilities.attackHitbox.enabled = value == 1;
    }
    /// <summary>
    /// sets can melee to <paramref name="value"/>
    /// </summary>
    /// <param name="value"></param>
    public void SetCanMelee(int value)
    {
        abilities.canAttack = value == 1;
    }
}
