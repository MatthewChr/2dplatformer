﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// Author: Matthew Christiansen
/// email: 17490@my4county.net
/// <summary>
/// Controls the boss
/// </summary>
public class BossAI : MonoBehaviour
{
    [SerializeField] bool active;
    [SerializeField] Transform bulletPos;
    [SerializeField] GameObject bulletPref;
    [SerializeField] float bulletSpeed = 15;
    [SerializeField] int prevAction;
    [SerializeField] bool paused;
    [SerializeField] float pauseTime = 2;
    [SerializeField] ParticleSystem alertParticle;

    [SerializeField] RectTransform HPBar;
    [Space]
    [SerializeField] protected Rigidbody2D[] limbs;
    [SerializeField] protected PolygonCollider2D[] limbColliders;


    float size;
    private enum BossState
    {
        attack, pause, ready, dead
    }
    [SerializeField] BossState state;

    Animator anim;
    Rigidbody2D rb;
    /// <summary>
    /// runs on start
    /// </summary>
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        //rb.velocity = new Vector2(0, 2);
        anim = GetComponent<Animator>();
        state = BossState.pause;
        size = HPBar.sizeDelta.x;

        foreach (Rigidbody2D r in limbs)
        {
            r.bodyType = RigidbodyType2D.Kinematic;
        }
        foreach (PolygonCollider2D c in limbColliders)
        {
            c.enabled = false;
        }
        rb.freezeRotation = true;
    }



    /// <summary>
    /// Update is called once per frame
    /// </summary>
    void FixedUpdate()
    {
        if (active)
        {
            switch (state)
            {
                case BossState.attack:
                    break;
                case BossState.pause:
                    if (!paused)
                        StartCoroutine(PauseTimer());
                    break;
                case BossState.ready:
                    PickAttack();
                    break;
            }
        }
    }
    /// <summary>
    /// picks a random attack and plays it
    /// </summary>
    private void PickAttack()
    {
        state = BossState.attack;

        int action = -1;

        do
        {
            action = Random.Range(0, 4);
        }
        while (action == prevAction || action == -1);
        prevAction = action;

        anim.SetInteger("AttackType", action);
        anim.SetTrigger("Attack");

        StartCoroutine(AttackCooldown(anim.GetCurrentAnimatorStateInfo(0).length));
    }
    /// <summary>
    /// pauses the boss for a moment between attacks
    /// </summary>
    IEnumerator PauseTimer()
    {
        paused = true;
        yield return new WaitForSeconds(pauseTime - 1);
        alertParticle.Play();
        yield return new WaitForSeconds(1);
        state = BossState.ready;
        paused = false;
    }
    /// <summary>
    /// waits for the boss to finish its current time
    /// </summary>
    /// <param name="cooldown"></param>
    IEnumerator AttackCooldown(float cooldown = 3)
    {
        yield return new WaitForSeconds(cooldown);
        state = BossState.pause;
    }
    /// <summary>
    /// fires the gun
    /// </summary>
    public void FireGun()
    {
        Instantiate(bulletPref, bulletPos.position, Quaternion.identity, null).GetComponent<Bullet>().SetData(bulletPos.right * transform.localScale.x, bulletSpeed);//Vector2.Scale(bulletPos.right, new Vector2(transform.localScale.x, 1)), bulletSpeed);
    }
    /// <summary>
    /// activates the boss AI
    /// </summary>
    public void Activate()
    {
        active = true;
    }
    /// <summary>
    /// updates the HP bar
    /// </summary>
    public void UpdateHP()
    {
        Health hp = GetComponent<Health>();
        HPBar.sizeDelta = new Vector2(size * ((float)hp.currentHP / hp.maxHP), HPBar.sizeDelta.y);
    }
    /// <summary>
    /// kills the boss
    /// </summary>
    public void Die()
    {
        Debug.Log(gameObject.name + " is dead");
        StartCoroutine(DieTimer());
    }
    /// <summary>
    /// kills the boss
    /// </summary>
    private IEnumerator DieTimer()
    {
        StopCoroutine(AttackCooldown());
        StopCoroutine(PauseTimer());

        state = BossState.dead;
        anim.enabled = false;
        rb.freezeRotation = false;
        rb.bodyType = RigidbodyType2D.Dynamic;
        //explodes limbs
        foreach (Collider2D c in GetComponents<Collider2D>())
        {
            c.enabled = false;
        }

        foreach (Rigidbody2D r in limbs)
        {
            r.bodyType = RigidbodyType2D.Dynamic;
        }
        foreach (PolygonCollider2D c in limbColliders)
        {
            c.transform.SetParent(transform);
            c.enabled = true;
        }

        yield return new WaitForSeconds(4);
        gameObject.SetActive(false);
    }
    /// <summary>
    /// restarts the boss fight
    /// </summary>
    public void Restart()
    {
        StartCoroutine(RestartTimer());
    }
    /// <summary>
    /// a timer that restarts the boss fight
    /// </summary>
    IEnumerator RestartTimer()
    {
        yield return new WaitForSeconds(5);
        Health hp = GetComponent<Health>();
        hp.currentHP = hp.maxHP;
        state = BossState.pause;
    }
}
