﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// Author: Matthew Christiansen
/// email: 17490@my4county.net
/// <summary>
/// manages the health of an entity
/// </summary>
public class Health : MonoBehaviour
{
    public bool invulernable;
    public int maxHP;
    public int currentHP;
    [SerializeField] float hitInvul = 0;
    public UnityEvent OnDamaged;
    public UnityEvent OnDeath;
    public UnityEvent OnHeal;
    /// <summary>
    /// runs on start
    /// </summary>
    private void Start()
    {
        currentHP = maxHP;
    }
    /// <summary>
    /// takes <paramref name="ammount"/> ammount of damage
    /// </summary>
    /// <param name="ammount"></param>
    public void TakeDamage(int ammount = 1)
    {
        if (invulernable)
            return;

        currentHP -= ammount;

        if (hitInvul > 0)
            StartCoroutine(hitInvulDelay());

        OnDamaged.Invoke();

        if (currentHP <= 0)
        {
            Die();
            currentHP = 0;
        }
    }
    /// <summary>
    /// heals the player by <paramref name="amt"/>
    /// </summary>
    /// <param name="amt"></param>
    /// <returns>returns false if health is full</returns>
    public bool Heal(int amt = 1)
    {
        if (currentHP < maxHP)
        {
            currentHP += amt;
            OnHeal.Invoke();
            return true;
        }
        else
            return false;
    }
    /// <summary>
    /// gives the entity a window where they cant be hurt
    /// </summary>
    IEnumerator hitInvulDelay()
    {
        invulernable = true;
        yield return new WaitForSeconds(hitInvul);
        invulernable = false;
    }
    /// <summary>
    /// kills the entity
    /// </summary>
    public void Die()
    {
        OnDeath.Invoke();
        Debug.Log("Die");
    }
}
