﻿using UnityEngine;
/// Author: Matthew Christiansen
/// email: 17490@my4county.net
/// <summary>
/// holds the data to be saved
/// </summary>
[System.Serializable]
public class GameSaveData
{
    public int lives = 5;
    public float jetpackFuel = 25;
    public bool level2;
    public bool hasMechArm;
    public int salvageCount;
    /// <summary>
    /// sets the values of this object to that of "<paramref name="data"/>"
    /// </summary>
    /// <param name="data"></param>
    public void SetData(GameSaveData data)
    {
        jetpackFuel = data.jetpackFuel;
        level2 = data.level2;
        hasMechArm = data.hasMechArm;
        salvageCount = data.salvageCount;
    }
    /// <summary>
    /// returns the values of the object as a string
    /// </summary>
    public override string ToString()
    {
        return lives + ", " + jetpackFuel;
    }
}