﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// Author: Matthew Christiansen
/// email: 17490@my4county.net
/// <summary>
/// Scrap pickup
/// </summary>
public class ScrapItem : MonoBehaviour
{
    Transform player;
    static float speed = 10;
    /// <summary>
    /// runs on start
    /// </summary>
    private void Start()
    {
        player = FindObjectOfType<PlayerMovement>().transform;
        Invoke("Pickup", 1);
    }
    /// <summary>
    /// runs every frame
    /// </summary>
    private void Update()
    {
        //moves toward the player
        transform.position = Vector3.Lerp(transform.position, player.position, Time.deltaTime * speed);
    }
    /// <summary>
    /// adds one scrap to the player's inventory then destroys itself
    /// </summary>
    private void Pickup()
    {
        player.GetComponent<PlayerAbilities>().AddSalvage();
        Destroy(gameObject);
    }
}
