﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// Author: Matthew Christiansen
/// email: 17490@my4county.net
/// <summary>
/// closes the door and starts the boss fight
/// </summary>
public class BossRoom : MonoBehaviour
{
    [SerializeField] bool bossActive;
    Animator anim;
    [SerializeField] BossAI boss;
    /// <summary>
    /// runs on start
    /// </summary>
    private void Start()
    {
        anim = GetComponent<Animator>();
    }
    /// <summary>
    /// called when this object's collider enter <paramref name="other"/>
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log(other);

        if (!bossActive)
            if (other.GetComponent<PlayerMovement>())
            {
                StartBoss();
            }
    }
    /// <summary>
    /// starts the boss
    /// </summary>
    private void StartBoss()
    {
        bossActive = true;
        anim.SetTrigger("Start");
        boss.Activate();
    }
    /// <summary>
    /// restarts the boss fight
    /// </summary>
    public void ResetBoss()
    {
        boss.Restart();
    }
    /// <summary>
    /// saves the boss before loading to the hub
    /// </summary>
    public void OnBossKill()
    {
        FindObjectOfType<PlayerAbilities>().UnlockLevel2();
        FindObjectOfType<PlayerAbilities>().SaveCurrentData();
        StartCoroutine(HubLoad());
    }
    /// <summary>
    /// delays loading to the hub world for a few moments
    /// </summary>
    IEnumerator HubLoad()
    {
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene(1);
    }
}
