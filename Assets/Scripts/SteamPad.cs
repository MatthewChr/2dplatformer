﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// Author: Matthew Christiansen
/// email: 17490@my4county.net
/// <summary>
/// Script that recharges the player's fuel
/// </summary>
public class SteamPad : MonoBehaviour
{
    //the speed at which the fuel refills
    [SerializeField] float fillRate = 10;
    PlayerAbilities abilities;
    /// <summary>
    /// Runs once when it enters <paramref name="other"/>
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<PlayerAbilities>() && abilities == null)
        {
            abilities = other.GetComponent<PlayerAbilities>();
        }
    }
    /// <summary>
    /// runs every physics update and adds fuel if player is touching it
    /// </summary>
    private void FixedUpdate()
    {
        if (abilities)
        {
            abilities.AddBoostFuel(fillRate * Time.deltaTime);
        }
    }
    /// <summary>
    /// Runs once when it exits <paramref name="other"/>
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.GetComponent<PlayerAbilities>() && abilities == other.GetComponent<PlayerAbilities>())
        {
            abilities = null;
        }
    }
}
