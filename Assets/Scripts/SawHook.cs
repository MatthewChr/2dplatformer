﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// Author: Matthew Christiansen
/// email: 17490@my4county.net
/// <summary>
/// Organizes and accesses the saws and the line renderers used to connect them to the hands
/// </summary>
public class SawHook : MonoBehaviour
{
    [SerializeField] Transform[] hands;
    [SerializeField] Transform[] saws;
    [SerializeField] LineRenderer[] lines;

    /// <summary>
    /// runs on start
    /// </summary>
    private void Start()
    {
        //disables all saws at the begining
        for (int i = 0; i < 4; i++)
        {
            DisableSaw(i);
        }
    }
    /// <summary>
    /// runs on every frame
    /// </summary>
    void Update()
    {
        //updates the positions of the linerenderers
        for (int i = 0; i < saws.Length; i++)
        {
            lines[i].SetPositions(new Vector3[] { hands[i].position, saws[i].position });
        }
    }
    /// <summary>
    /// Calls for the saw and linerender at <paramref name="index"/> to be enabled
    /// </summary>
    /// <param name="index"></param>
    public void EnableSaw(int index)
    {
        saws[index].gameObject.SetActive(true);
        lines[index].enabled = true;
    }
    /// <summary>
    /// Calls for the saw and linerender at <paramref name="index"/> to be disabled
    /// </summary>
    /// <param name="index"></param>
    public void DisableSaw(int index)
    {
        saws[index].gameObject.SetActive(false);
        lines[index].enabled = false;
    }
}
