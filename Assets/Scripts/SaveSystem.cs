﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

/// Author: Matthew Christiansen
/// email: 17490@my4county.net
/// <summary>
/// The save system used to save and load data
/// </summary>
public static class SaveSystem
{
    private static GameSaveData saveData;
    private static int saveSlot = 0;
    /// <summary>
    /// gets the current save data
    /// </summary>
    /// <returns>The current save data</returns>
    public static GameSaveData GetSaveData()
    {
        if (CheckForSave())
            return LoadFile();
        else
            return CreateSave();
    }
    /// <summary>
    /// Sets the save data "<paramref name="data"/>" as the current data
    /// </summary>
    /// <param name="data"></param>
    public static void SetSaveData(GameSaveData data)
    {
        saveData = data;
    }
    /// <summary>
    /// sets the current save slot to <paramref name="index"/>
    /// </summary>
    /// <param name="index"></param>
    public static void SetSaveSlot(int index)
    {
        saveSlot = index;
    }
    /// <summary>
    /// Checks and returns true if data in slot <paramref name="index"/> exists
    /// </summary>
    /// <param name="index"></param>
    /// <returns></returns>
    public static bool CheckForSave(int index = -1)
    {
        string savePath;
        if (index == -1)
            savePath = Application.persistentDataPath + "gamesave" + saveSlot + ".save";
        else
            savePath = Application.persistentDataPath + "gamesave" + index + ".save";
        return File.Exists(savePath);
    }
    /// <summary>
    /// Creates and returns save data in current save slot
    /// </summary>
    /// <returns></returns>
    public static GameSaveData CreateSave()
    {
        string savePath = Application.persistentDataPath + "gamesave" + saveSlot + ".save";
        saveData = new GameSaveData();
        BinaryFormatter formatter = new BinaryFormatter();
        using (FileStream stream = File.Create(savePath))
        {
            try
            {
                formatter.Serialize(stream, saveData);
            }
            catch (System.IO.IOException ioe)
            {
                Debug.Log(ioe);
                return null;
            }
        }
        Debug.Log("saved new in slot " + saveSlot);
        return saveData;
    }
    /// <summary>
    /// Saves the current save data to file in current slot
    /// </summary>
    public static void SaveFile()
    {
        string savePath = Application.persistentDataPath + "gamesave" + saveSlot + ".save";
        BinaryFormatter formatter = new BinaryFormatter();
        using (FileStream stream = File.Create(savePath))
        {
            try
            {
                formatter.Serialize(stream, saveData);
            }
            catch (System.IO.IOException ioe)
            {
                Debug.Log(ioe);
                return;
            }
        }
        Debug.Log("saved in slot " + saveSlot);
    }
    /// <summary>
    /// Loads and returns save data in current slot
    /// </summary>
    /// <returns></returns>
    public static GameSaveData LoadFile()
    {
        string savePath = Application.persistentDataPath + "gamesave" + saveSlot + ".save";
        if (File.Exists(savePath))
        {
            Debug.Log("file found\nLoading in slot " + saveSlot);
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream stream = File.Open(savePath, FileMode.Open))
            {
                saveData = (GameSaveData)formatter.Deserialize(stream);
                return saveData;
            }
        }
        else
        {
            Debug.Log("file not found");
            return null;
        }
    }
    /// <summary>
    /// Deletes slot at index "<paramref name="index"/>"
    /// </summary>
    /// <param name="index"></param>
    public static void DeleteSaveSlot(int index)
    {
        string savePath = Application.persistentDataPath + "gamesave" + index + ".save";
        if (File.Exists(savePath))
        {
            File.Delete(savePath);
            Debug.Log("Save " + index + " was deleted");
        }
        else
            Debug.Log("Save " + index + " couldnt be deleted, no file exists");
    }
    /// <summary>
    /// creates save slot at index 0 for testing purposes
    /// </summary>
    [ContextMenu("Create Save 0")]
    public static void CreateSave0()
    {
        string savePath = Application.persistentDataPath + "gamesave0.save";
        BinaryFormatter formatter = new BinaryFormatter();
        using (FileStream stream = File.Create(savePath))
        {
            try
            {
                formatter.Serialize(stream, new GameSaveData());
            }
            catch (System.IO.IOException ioe)
            {
                Debug.Log(ioe);
                return;
            }
        }
        Debug.Log("saved in slot 0");
    }
}
