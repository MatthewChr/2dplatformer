﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// Author: Matthew Christiansen
/// email: 17490@my4county.net
/// <summary>
/// controls the player's melee, jetpack, and healthbar
/// </summary>
public class PlayerAbilities : MonoBehaviour
{
    [SerializeField] bool dead;
    [SerializeField] Vector2 mousePos;
    [SerializeField] Vector2 mouseDir;
    Vector2 inputDir;
    [SerializeField] private PlayerMovement playerMovement;
    [SerializeField] private GameSaveData saveData;
    [Header("Attack")]
    [SerializeField] private SpriteRenderer attackParticle;
    public bool canAttack;
    public Collider2D attackHitbox;
    [Header("Health")]
    Health hp;
    [SerializeField] RectTransform hpBar;
    float hpBarWidth;
    [Header("Jetpack")]
    public Vector2 boostRatio = Vector2.one;
    public Vector2 boostVel;
    public float boostSpeed;
    public float boostJuice = 100;
    public float boostJuiceMax = 100;
    public float boostDrain;
    public bool boostCooldown;
    [Space]
    public ParticleSystem boostSteam;
    public Transform steamDir;
    public Material steamMat;
    [SerializeField] float steamMatTile;
    [SerializeField] RectTransform barRect;
    [SerializeField] Image cantBoostGraphic;
    [SerializeField] Animator anim;
    /// <summary>
    /// runs on start
    /// </summary>
    void Start()
    {
        SetUp();
    }
    /// <summary>
    /// sets up the player to be ready
    /// </summary>
    public void SetUp()
    {
        hp = GetComponent<Health>();
        saveData = SaveSystem.GetSaveData();
        UpdateAbilityData();

        hpBarWidth = hpBar.sizeDelta.x;

        boostJuice = boostJuiceMax;
        cantBoostGraphic.enabled = false;
        canAttack = true;
    }
    /// <summary>
    /// runs every frame
    /// </summary>
    void Update()
    {
        if (dead)
            return;
        GetMousePosition();
        JetpackBehavior();

        if (Input.GetKeyDown(KeyCode.Mouse0) && canAttack)
        {
            anim.SetTrigger("Attack");
        }
    }
    /// <summary>
    /// finds the mouse position and direction relative to the player
    /// </summary>
    private void GetMousePosition()
    {
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mouseDir = (mousePos - (Vector2)transform.position).normalized;
    }
    /// <summary>
    /// saves the player data to the SaveSystem
    /// </summary>
    public void SaveCurrentData()
    {
        SaveSystem.SetSaveData(saveData);
        SaveSystem.SaveFile();
    }
    /// <summary>
    /// unlocks level 2 for the player
    /// </summary>
    public void UnlockLevel2()
    {
        saveData.level2 = true;
    }
    /// <summary>
    /// adds to the player's salvage count
    /// </summary>
    public void AddSalvage()
    {
        saveData.salvageCount++;
    }

    #region Jetpack
    /// <summary>
    /// controls the jetpack
    /// </summary>
    private void JetpackBehavior()
    {
        inputDir = mouseDir;

        //checks to see if the player can boost
        if (Input.GetKey(KeyCode.Mouse1) && !boostCooldown)
        {
            steamDir.localScale = transform.localScale;
            if (boostJuice > 0)
            {
                //drains fuel
                boostJuice -= boostDrain * Time.deltaTime;

                //stops if the player from boosting if they're out of fuel and puts it on a cooldown
                if (boostJuice < 0 || (Mathf.Abs(boostJuice) < Mathf.Epsilon && Input.GetKey(KeyCode.Mouse1)))
                {
                    boostJuice = 0;
                    boostCooldown = true;
                }

                if (boostSteam.isStopped)
                    boostSteam.Play();

                //rotates the steam particle to face the direction of the mouse
                if (Mathf.Abs(inputDir.sqrMagnitude) >= Mathf.Epsilon)
                {
                    float angle = Mathf.Atan2(inputDir.normalized.y, inputDir.normalized.x) * Mathf.Rad2Deg;
                    steamDir.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
                }
                else
                {
                    steamDir.right = Vector2.up;
                }

                boostVel = inputDir.normalized * boostSpeed * boostRatio;
            }

            else
            {
                StartCoroutine(boostAntiSpam());
            }
        }
        else //not boosting
        {
            if (boostSteam.isPlaying)
                boostSteam.Stop();
            boostVel = Vector2.zero;
        }
        //when the steam bar is at 1/4 or more it will allow the player to boost again
        if (boostCooldown && boostJuice >= boostJuiceMax / 4)
            boostCooldown = false;

        cantBoostGraphic.enabled = boostCooldown;

        //updates the steam bar and it's shader to show how much fuel is left
        barRect.localScale = new Vector3(boostJuice / boostJuiceMax, 1, 1);
        steamMat.SetVector("_Tiling", new Vector4(steamMatTile * boostJuice / boostJuiceMax, steamMat.GetVector("_Tiling").y));
        playerMovement.SetAdditionalVel(boostVel, Input.GetKey(KeyCode.Mouse1) && !boostCooldown);
    }
    /// <summary>
    /// Adds fuel to jetpack
    /// </summary>
    /// <param name="amt">ammount to add</param>
    public void AddBoostFuel(float amt)
    {
        boostJuice += amt;
        if (boostJuice > boostJuiceMax)
            boostJuice = boostJuiceMax;
    }
    /// <summary>
    /// Updates players stats
    /// </summary>
    public void UpdateAbilityData()
    {
        boostJuiceMax = saveData.jetpackFuel;
        hp.maxHP = saveData.lives;
    }
    /// <summary>
    /// returns the savedata of the player
    /// </summary>
    /// <returns>savedata</returns>
    public GameSaveData GetData()
    {
        return saveData;
    }
    /// <summary>
    /// sets the player's save data
    /// </summary>
    /// <param name="data"></param>
    public void SetData(GameSaveData data)
    {
        this.saveData = data;
    }
    /// <summary>
    /// prevents the player from rapidly toggling boosting
    /// </summary>
    IEnumerator boostAntiSpam()
    {
        boostVel = Vector2.zero;
        boostCooldown = true;
        yield return new WaitForSeconds(1);
        Debug.Log("Boost Cooldown Over");
        boostCooldown = false;
    }
    #endregion
    #region Health
    /// <summary>
    /// updates the UI to show player's health
    /// </summary>
    public void UpdateCurrentHP()
    {
        float hpPercent = (float)hp.currentHP / hp.maxHP;
        Debug.Log(System.Math.Round(hpPercent * 100, 3) + "%");
        hpBar.sizeDelta = new Vector2(hpPercent * hpBarWidth, hpBar.sizeDelta.y);
    }
    /// <summary>
    /// public access to the player death
    /// </summary>
    public void Die()
    {
        StartCoroutine(OnDie());
    }
    /// <summary>
    /// kills the player and loads hub after a moment
    /// </summary>
    IEnumerator OnDie()
    {
        SaveSystem.SaveFile();
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.freezeRotation = false;
        boostJuice = 0;
        playerMovement.enabled = false;
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene(1);
    }
    #endregion
    /// <summary>
    /// resets steam bar tiling
    /// </summary>
    private void OnDestroy()
    {
        steamMat.SetVector("_Tiling", new Vector4(8,1,0,0));
    }
    /// <summary>
    /// debug tool
    /// </summary>
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position, transform.position + (Vector3)mouseDir);
    }
}
