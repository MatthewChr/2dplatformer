﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// Author: Matthew Christiansen
/// email: 17490@my4county.net
/// <summary>
/// heals the player when it enters the trigger
/// </summary>
public class HealthPack : MonoBehaviour
{
    [SerializeField] int amt = 1;
    /// <summary>
    /// called when this object's collider enters <paramref name="other"/>
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<PlayerMovement>())
        {
            if (other.GetComponent<Health>().Heal(amt))
            {
                Destroy(gameObject);
            }
        }
    }
}
