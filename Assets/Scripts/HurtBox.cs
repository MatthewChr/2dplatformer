﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// Author: Matthew Christiansen
/// email: 17490@my4county.net
/// <summary>
/// does damage and knockback on enemy that enters the trigger
/// </summary>
public class HurtBox : MonoBehaviour
{
    public float force = 10;
    /// <summary>
    /// called when this object's collider enters <paramref name="other"/>
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<Health>())
        {
            other.GetComponent<Health>().TakeDamage();

            Vector2 forceDir = (other.transform.position - transform.position).normalized;

            other.GetComponent<Rigidbody2D>().velocity += (forceDir * force);
        }
    }
}
