﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// Author: Matthew Christiansen
/// email: 17490@my4county.net
/// <summary>
/// controls the bullet damage and movement
/// </summary>
public class Bullet : MonoBehaviour
{
    Rigidbody2D rb;
    float speed = 5;
    /// <summary>
    /// runs on object creation
    /// </summary>
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    /// <summary>
    /// sets the bullet speed as "<paramref name="speed"/>" and direction as "<paramref name="dir"/>"
    /// </summary>
    /// <param name="dir"></param>
    /// <param name="speed"></param>
    public void SetData(Vector2 dir, float speed = 5)
    {
        transform.up = dir;
        rb.velocity = transform.up * speed;
        Destroy(gameObject, 10);
    }
    /// <summary>
    /// checks if the bullet has collided with "<paramref name="other"/>"
    /// </summary>
    /// <param name="other"></param>
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.GetComponent<Health>())
        {
            other.collider.GetComponent<Health>().TakeDamage();
            Debug.Log("Hit");
        }
        else
        {
            Debug.Log("hit wall");
        }
        Destroy(gameObject);
    }
}
