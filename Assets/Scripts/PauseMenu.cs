﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Audio;

/// Author: Matthew Christiansen
/// email: 17490@my4county.net
/// <summary>
/// Handles the pause menu
/// </summary>
public class PauseMenu : MonoBehaviour
{
    [SerializeField] bool paused;
    [SerializeField] GameObject pauseMenu;
    [SerializeField] TextMeshProUGUI salvageTxt;
    [SerializeField] bool musicMuted;
    AudioSource audioSource;
    /// <summary>
    /// runs on start
    /// </summary>
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        pauseMenu.SetActive(false);
    }
    /// <summary>
    /// runs when level is loaded
    /// </summary>
    private void OnLevelWasLoaded(int level)
    {
        FindObjectOfType<PlayerAbilities>().SetUp();
    }
    /// <summary>
    /// runs every frame
    /// </summary>
    private void Update()
    {
        //mutes music
        if (Input.GetKeyDown(KeyCode.M))
        {
            musicMuted = !musicMuted;

            if (!musicMuted)
                audioSource.Play();
            else
                audioSource.Stop();
        }
        //pauses/unpauses the game
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            paused = !paused;
            pauseMenu.SetActive(paused);
            Time.timeScale = paused ? 0 : 1;
            salvageTxt.text = "Salvage: " + SaveSystem.GetSaveData().salvageCount.ToString();
        }
    }
    /// <summary>
    /// Saves game data
    /// </summary>
    public void Save()
    {
        FindObjectOfType<PlayerAbilities>().SaveCurrentData();
        SceneManager.LoadScene(0);
    }
    /// <summary>
    /// exits to main menu
    /// </summary>
    public void Exit()
    {
        Save();
        SceneManager.LoadScene(0);
    }
}
