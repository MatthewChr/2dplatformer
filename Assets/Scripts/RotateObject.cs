﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// Author: Matthew Christiansen
/// email: 17490@my4county.net
/// <summary>
/// rotates the gameobject this is attached to
/// </summary>
public class RotateObject : MonoBehaviour
{
    public float rotationAmt = 5;
    /// <summary>
    /// runs on physics update
    /// </summary>
    void FixedUpdate()
    {
        float z = (transform.localEulerAngles + new Vector3(0, 0, rotationAmt)).z;

        if (z > 0)
        {
            if (z > -360)
                z += 360;
        }
        else
        {
            if (z > 360)
                z -= 360;
        }

        transform.localEulerAngles = new Vector3(0, 0, z);
    }
}
