﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// Author: Matthew Christiansen
/// email: 17490@my4county.net
/// <summary>
/// a branch of the enemyAI class that uses ranged weapons
/// </summary>
public class RangedEnemyAI : EnemyAI
{
    [Header("Ranged")]
    [SerializeField] Transform arms;
    [SerializeField] Transform laserObj;
    [SerializeField] Transform barrelEnd;
    [SerializeField] GameObject bullet;
    [SerializeField] float aimOffset;
    [SerializeField] float bulletSpeed = 7;
    [SerializeField] float aimSmoothing = 10;
    [SerializeField] float shootAngleAllowance;
    [SerializeField] float aimTime = 1.5f;
    [SerializeField] bool isAiming;
    [SerializeField] bool fireCooldown;
    [SerializeField] float shootCooldown = 1;
    [SerializeField] LayerMask aimDetectMask;
    /// <summary>
    /// runs on physics update
    /// </summary>
    protected void FixedUpdate()
    {
        if (state != EnemyState.chase && state != EnemyState.attacking)
        {
            isAiming = false;
        }
    }
    /// <summary>
    /// Override of the attack method from enemyAI
    /// </summary>
    protected override void Attack()
    {
        if (Vector3.Distance(transform.position, player.position) - attackRange/8 > attackRange)
        {
            state = EnemyState.chase;
            return;
        }

        bool facingRight = (player.position - transform.position).x >= 0;

        //sets scale to make the nemy face the right way
        transform.localScale = facingRight ? new Vector3(1, 1, 1) : new Vector3(-1, 1, 1);
        SetSpeed(0, acceleration);

        //calculates the angle and rotation to make the arms face the player
        Vector2 dir = player.position - transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        Quaternion rotGoal = facingRight ? Quaternion.AngleAxis(angle + aimOffset, Vector3.forward) : Quaternion.AngleAxis(angle + aimOffset - 180, Vector3.forward);
        arms.rotation = Quaternion.Lerp(arms.rotation, rotGoal, Time.deltaTime * (1 + aimSmoothing));

        //checks for line-of-sight
        if (Physics2D.Linecast(transform.position, player.position, aimDetectMask))
        {
            if (isAiming)
            {
                isAiming = false;
                StopCoroutine(ShootGun());
            }
        }
        else
        {
            if (!isAiming)
            {
                StartCoroutine(ShootGun());
                Debug.Log("Start Aim");
            }
        }
        anim.SetBool("Attack", isAiming);
        laserObj.gameObject.SetActive(isAiming);

        //shows laser when aiming and hides it when not aiming
        if (isAiming)
            laserObj.localScale = new Vector3(Vector3.Distance(laserObj.position, player.position), laserObj.localScale.y);
        else
            laserObj.localScale = new Vector3(0, laserObj.localScale.y);
    }
    /// <summary>
    /// a timer for aiming and shooting
    /// </summary>
    IEnumerator ShootGun()
    {
        isAiming = true;
        yield return new WaitForSeconds(aimTime);
        laserObj.localScale = new Vector3(0, laserObj.localScale.y);
        if (!fireCooldown)
        {
            fireCooldown = true;
            isAiming = false;
            //creates and points bullet in the direction the enemy is facing
            Instantiate(bullet, barrelEnd.position, barrelEnd.rotation).GetComponent<Bullet>().SetData(barrelEnd.right * transform.localScale.x, bulletSpeed);
            yield return new WaitForSeconds(shootCooldown);
            fireCooldown = false;
            Debug.Log("Fire");
        }
    }
    /// <summary>
    /// disassembles the enemy and then disables it
    /// </summary>
    protected override IEnumerator DieTimer()
    {
        StopCoroutine(ShootGun());
        laserObj.gameObject.SetActive(false);
        StartCoroutine(base.DieTimer());
        yield return null;
    }
}
