﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;

/// Author: Matthew Christiansen
/// email: 17490@my4county.net
/// <summary>
/// Main menu script for loading the game
/// </summary>
public class Menu : MonoBehaviour
{
    [SerializeField] GameObject buttonPref;
    [SerializeField] TextMeshProUGUI[] saveText;
    [SerializeField] GameObject[] deleteButtons;
    [SerializeField] GameObject[] screens;
    [SerializeField] GameObject backButton;
    /// <summary>
    /// runs on start
    /// </summary>
    private void Start()
    {
        UpdateSlotListings();
        ShowScreen(0);
    }
    /// <summary>
    /// Loads the save data (or creates it if it doesnt exist) in the slot number <paramref name="index"/>
    /// </summary>
    /// <param name="index"></param>
    public void LoadSave(int index)
    {
        SaveSystem.SetSaveSlot(index);
        if (SaveSystem.CheckForSave())
            SaveSystem.LoadFile();
        else
            SaveSystem.CreateSave();

        LoadLevel(index);
    }
    /// <summary>
    /// deletes save file in slot <paramref name="index"/>
    /// </summary>
    /// <param name="index"></param>
    public void DeleteSlot(int index)
    {
        SaveSystem.DeleteSaveSlot(index);
        UpdateSlotListings();
    }
    /// <summary>
    /// loads level at scene index "<paramref name="index"/>"
    /// </summary>
    /// <param name="index"></param>
    public static void LoadLevel(int index)
    {
        SceneManager.LoadScene(index);
    }
    /// <summary>
    /// shows the level load window on main menu
    /// </summary>
    public void ShowLeveLoad()
    {
        UpdateSlotListings();
    }
    /// <summary>
    /// shows window index "<paramref name="index"/>" and hides the others
    /// </summary>
    /// <param name="index"></param>
    public void ShowScreen(int index)
    {
        for (int i = 0; i < screens.Length; i++)
        {
            screens[i].SetActive(i == index);
        }

        backButton.SetActive(index != 0);
    }
    /// <summary>
    /// updates the UI to show if there is a save in the slot
    /// </summary>
    public void UpdateSlotListings()
    {
        for (int i = 0; i < 3; i++)
        {
            bool saveFound = SaveSystem.CheckForSave(i + 1);
            saveText[i].text = saveFound ? "Load Save " + (i+1) : "Create Save " + (i+1);
            deleteButtons[i].SetActive(saveFound);
        }
    }
    /// <summary>
    /// quits the application
    /// </summary>
    public void Quit()
    {
        Application.Quit();
    }
}
