﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// Author: Matthew Christiansen
/// email: 17490@my4county.net
/// <summary>
/// controls the enemy AI
/// </summary>
public class EnemyAI : MonoBehaviour
{
    protected enum EnemyType
    {
        basic,
        ranged,
        buff
    }
    protected enum EnemyState
    {
        stand,
        patrol,
        chase,
        stun,
        dead,
        attacking
    }
    [Header("Base")]
    [SerializeField] protected float maxTrackRange;
    [SerializeField] protected float stopDistance;
    [SerializeField] protected float attackRange;
    [SerializeField] protected float moveSpeed = 5;
    [SerializeField] protected float chaseSpeed;
    [SerializeField] protected bool paused;
    [SerializeField] protected float pauseTime = 0.75f;
    [SerializeField] protected float acceleration = 2f;
    [SerializeField] protected Transform player;
    [SerializeField] protected EnemyState state;
    [SerializeField] protected EnemyState preferedState;
    [SerializeField] protected EnemyType type;
    [SerializeField] protected Collider2D footCol;
    [SerializeField] protected bool grounded;
    [SerializeField] protected Collider2D wallDetect, jumpDetect, endOfPlatformDetect;
    [SerializeField] protected Collider2D detectCone;
    [SerializeField] protected bool foundPlayer;
    [SerializeField] protected ParticleSystem alertParticle;
    [Header("Ragdoll")]
    [SerializeField] protected Rigidbody2D[] limbs;
    [SerializeField] protected PolygonCollider2D[] limbColliders;
    [SerializeField] protected Collider2D meleeHitbox;
    [Space]
    [SerializeField] protected GameObject scrapPref;
    protected Animator anim;
    Rigidbody2D rb;
    /// <summary>
    /// runs on start
    /// </summary>
    protected void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();

        //disables the explosion parts
        player = FindObjectOfType<PlayerMovement>().transform;

        foreach (Rigidbody2D r in limbs)
        {
            r.bodyType = RigidbodyType2D.Kinematic;
        }
        foreach (PolygonCollider2D c in limbColliders)
        {
            c.enabled = false;
        }
        rb.freezeRotation = true;
    }
    /// <summary>
    /// runs every frame
    /// </summary>
    protected void Update()
    {
        grounded = Physics2D.IsTouchingLayers(footCol, LayerMask.GetMask("Terrain"));

        anim.SetBool("Walk", (grounded && Mathf.Abs(rb.velocity.x) > 0));

        //dictates what the enemy does in the state
        switch (state)
        {
            case EnemyState.stand:
                Stand();
                break;
            case EnemyState.patrol:
                Patrol();
                break;
            case EnemyState.chase:
                Chase();
                break;
            case EnemyState.stun:
                break;
            case EnemyState.dead:
                break;
            case EnemyState.attacking:
                Attack();
                break;
        }
    }
    /// <summary>
    /// looks for the player in front of them and checks if they are in line of sight
    /// </summary>
    protected void LookForPlayer()
    {
        //checks if player is in the detect trigger
        if (Physics2D.IsTouchingLayers(detectCone, LayerMask.GetMask("Player")))
        {
            //checks if in line of sight
            if (Physics2D.Linecast(new Vector2(transform.position.x, transform.position.y) + new Vector2(0.5f * transform.localScale.x, 0.8f * transform.localScale.y), player.transform.position))
            {
                foundPlayer = true;
            }
        }
        if (foundPlayer && Vector2.Distance(transform.position, player.position) > maxTrackRange)
            foundPlayer = false;

        if (foundPlayer)
        {
            state = EnemyState.chase;
            alertParticle.Play();
        }
    }
    /// <summary>
    /// the AI state of the nemy just standing
    /// </summary>
    protected void Stand()
    {
        LookForPlayer();
    }
    /// <summary>
    /// walks back and forth and turns around when it reaches a wall or cliff
    /// </summary>
    protected void Patrol()
    {
        LookForPlayer();
        //looks for cliff or wall
        if ((Physics2D.IsTouchingLayers(wallDetect, LayerMask.GetMask("Terrain")) ||
            (!Physics2D.IsTouchingLayers(endOfPlatformDetect, LayerMask.GetMask("Terrain")) && grounded)
            ) && !paused)
            StartCoroutine(TurnPause());

        if (paused)
            SetSpeed(0, acceleration);
        else
            SetSpeed(moveSpeed * transform.localScale.x);
    }
    /// <summary>
    /// chases the player
    /// </summary>
    protected void Chase()
    {
        bool canMove = !(!Physics2D.IsTouchingLayers(endOfPlatformDetect, LayerMask.GetMask("Terrain")) && grounded);

        transform.localScale = (player.position - transform.position).x >= 0 ? new Vector3(1, 1, 1) : new Vector3(-1, 1, 1);

        if (!canMove)
        {
            SetSpeed(0);

            if (Mathf.Abs((player.position - transform.position).x) <= attackRange)
                state = EnemyState.attacking;
            return;
        }
        //checks if the player is in range to be followed
        if (Mathf.Abs((player.position - transform.position).x) <= maxTrackRange)
        {
            if (Mathf.Abs((player.position - transform.position).x) > attackRange)
            {
                if ((player.position - transform.position).x >= 0)
                {
                    SetSpeed(chaseSpeed, acceleration);
                }
                else
                {
                    SetSpeed(-chaseSpeed, acceleration);
                }
            }
            else
            {
                SetSpeed(0, acceleration);
                state = EnemyState.attacking;
            }
        }
        else
            state = preferedState;
    }
    /// <summary>
    /// attacks the player if its in range
    /// </summary>
    protected virtual void Attack()
    {
        if (Mathf.Abs((player.position - transform.position).x) > attackRange)
        {
            state = EnemyState.chase;
            return;
        }
        anim.SetBool("Attack", true);
    }
    /// <summary>
    /// waits a small delay before turning around and walking
    /// </summary>
    protected IEnumerator TurnPause()
    {
        paused = true;
        yield return new WaitForSeconds(pauseTime);
        paused = false;
        transform.localScale = new Vector3(-transform.localScale.x, 1, 1);

    }
    /// <summary>
    /// sets the speed and acceleration for moving
    /// </summary>
    /// <param name="speed"></param>
    /// <param name="accel"></param>
    protected void SetSpeed(float speed, float accel = 10)
    {
        rb.velocity = Vector3.Lerp(rb.velocity, new Vector3(speed, rb.velocity.y), (1+accel) * Time.deltaTime);
    }
    /// <summary>
    /// debug tool
    /// </summary>
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        if (maxTrackRange > 0)
        Gizmos.DrawWireSphere(transform.position, maxTrackRange);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, attackRange);

    }
    /// <summary>
    /// how death is called by other scripts
    /// </summary>
    public void Die()
    {
        Debug.Log(gameObject.name + " is dead");
        StartCoroutine(DieTimer());
    }
    /// <summary>
    /// The enemy dies and explodes
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator DieTimer()
    {
        for (int i = 0; i < Random.Range(1,4); i++)
        {
            Vector3 rndOffset = new Vector3(Random.Range(-2f, 3f), Random.Range(-2f, 3f), 0);
            GameObject g = Instantiate(scrapPref, transform.position , Quaternion.identity);
        }
        state = EnemyState.dead;
        anim.enabled = false;
        rb.freezeRotation = false;

        //enables the physics for limbs
        foreach (Collider2D c in GetComponents<Collider2D>())
        {
            c.enabled = false;
        }

        foreach (Rigidbody2D r in limbs)
        {
            r.bodyType = RigidbodyType2D.Dynamic;
        }
        foreach (PolygonCollider2D c in limbColliders)
        {
            c.transform.SetParent(transform);
            c.enabled = true;
        }
        if (meleeHitbox)
            meleeHitbox.enabled = false;

        yield return new WaitForSeconds(4);
        gameObject.SetActive(false);
    }
}
