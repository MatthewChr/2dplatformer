﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// Author: Matthew Christiansen
/// email: 17490@my4county.net
/// <summary>
/// does damage when the trigger enters a collider
/// </summary>
public class MeleeHitbox : MonoBehaviour
{
    [SerializeField] private LayerMask layerMask;
    public int damage = 1;
    /// <summary>
    /// called when this object's collider enters <paramref name="other"/> 
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Hit");
        
        Health hp = other.gameObject.GetComponent<Health>();
        if (hp)
        {
            Debug.Log("Take Damage");
            hp.TakeDamage(damage);
        }
    }
}
