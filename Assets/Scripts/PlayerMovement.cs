﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// Author: Matthew Christiansen
/// email: 17490@my4county.net
/// <summary>
/// Controls player movement
/// </summary>
public class PlayerMovement : MonoBehaviour
{
    public float speed = 8;
    public float jumpForce = 15;
    public bool canJump;
    public float airMove = 0.3f;
    public float friction = 0.2f;
    public float highJumpMulti = 2;
    public float lowJumpMulti = 5f;
    public bool grounded;
    public Vector2 velocity;
    private Vector2 inputDir;
    public float cyoteTime = 0.1f;
    public Collider2D footCol;
    public float decayDelay = 0.3f;
    public bool decayJump;
    [Space]
    public Vector2 boostVel;
    public bool boosting;
    Rigidbody2D rb;
    SpriteRenderer sprite;
    [SerializeField] Animator anim;
    /// <summary>
    /// runs on start
    /// </summary>
    void Start()
    {
        boosting = false;
        rb = GetComponent<Rigidbody2D>();
    }
    /// <summary>
    /// runs every frame
    /// </summary>
    void Update()
    {
        inputDir = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        CheckGround();
        UpdateAnim();
    }
    /// <summary>
    /// updates the animator's values
    /// </summary>
    private void UpdateAnim()
    {
        anim.SetFloat("Y Velocity", Mathf.Clamp(Mathf.Abs(rb.velocity.y), 0, 5));
        anim.SetFloat("X Velocity", Mathf.Clamp01(Mathf.Abs(rb.velocity.x)));
        anim.SetBool("Boosting", boosting);
        PlayerFlip();
    }
    /// <summary>
    /// runs on physics update
    /// </summary>
    private void FixedUpdate()
    {
        Move();
    }
    /// <summary>
    /// checks if the plaeyr is grounded
    /// </summary>
    private void CheckGround()
    {
        bool g = Physics2D.IsTouchingLayers(footCol, LayerMask.GetMask("Terrain"));
        if (!g)
        {
            if (grounded)
            {
                StartCoroutine(CyoteTimer());
                grounded = false;
            }
        }
        else
        {
            grounded = g;
        }

    }
    /// <summary>
    /// Controls the movement
    /// </summary>
    private void Move()
    {
		if (grounded)
        {
            if (Input.GetKey(KeyCode.Space) && canJump)
            {
                rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            }

            rb.velocity = new Vector2(inputDir.x * speed, rb.velocity.y);
            canJump = true;
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Space) && canJump)
            {
                rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            }
            //adds air resistance to pthe player if there is no left/right input
            if (Mathf.Abs(inputDir.x) < Mathf.Epsilon)
                if (rb.velocity.x > 0)
                {
                    rb.velocity = new Vector2(Mathf.Clamp(rb.velocity.x - friction, 0, speed), rb.velocity.y);
                }
                else
                {
                    rb.velocity = new Vector2(Mathf.Clamp(rb.velocity.x + friction, -speed, 0), rb.velocity.y);
                }
            //if not using the jetpack there is air movement
            if (!boosting)
                rb.velocity += new Vector2(inputDir.x * speed * airMove * Time.deltaTime * 25, 0);

            if (boosting)
                rb.velocity = new Vector2(rb.velocity.x, 0);

            //if jump is being held then the player will fall slower
            if (!Input.GetKey(KeyCode.Space))
                rb.velocity -= new Vector2(0, Time.deltaTime * lowJumpMulti);
            else
                rb.velocity -= new Vector2(0, Time.deltaTime * highJumpMulti);
        }

        //clamps rigidbody velocity and adds the boost direction
        rb.velocity = new Vector2(Mathf.Clamp(rb.velocity.x, -speed, speed), rb.velocity.y) + boostVel;
        //shows inspector the player's velocity
        velocity = rb.velocity;
    }
    /// <summary>
    /// flips the character to face the direction he's moving in
    /// </summary>
    private void PlayerFlip()
    {
        if (boosting)
        {
            if (Mathf.Abs(inputDir.x) > Mathf.Epsilon)
            {
                transform.localScale = new Vector3(Mathf.Sign(boostVel.x), 1, 1);
            }
        }
        else
        {
            if (Mathf.Abs(inputDir.x) > Mathf.Epsilon)
            {
                transform.localScale = new Vector3(Mathf.Sign(inputDir.x), 1, 1);
            }
        }
    }
    /// <summary>
    /// sets the boosting direction to <paramref name="vel"/> and sets "boosting" to <paramref name="boosting"/>
    /// </summary>
    /// <param name="vel"></param>
    /// <param name="boosting"></param>
    public void SetAdditionalVel(Vector2 vel, bool boosting)
    {
        boostVel = vel;
        this.boosting = boosting;
    }
    /// <summary>
    /// allows for the player to jump for a short time after not touching the ground to make it control smoother
    /// </summary>
    IEnumerator CyoteTimer()
    {
        yield return new WaitForSeconds(cyoteTime);
        canJump = false;
    }
}


